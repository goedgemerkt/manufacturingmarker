import requests
from requests_oauthlib import OAuth1
import os
import xml.etree.ElementTree as ET

s = requests.Session()
log_file = open(r"C:/Projects/ManufacturingMarker/test_xml/log.txt", "a")
BASE_URL = 'https://live-shop.goedgemerkt.nl/index.php/rest/V1'
DIRECTORY = "C:/Projects/ManufacturingMarker/test_xml"

# Live system
CLIENT_KEY = 'ugmwk2l0hadb15jb5ncwpjucfbbjt15o'
CLIENT_SECRET = '4p37y45k4s9jybwq1qosgkpmyyucqqjt'
RESOURCE_OWNER_KEY = '1ump24d86ybnqdsufiujan3xt01anof8'
RESOURCE_OWNER_SECRET = '04ixo9k8qjjkx58c0fu1p3njev8j6tsw'


def admin_get_request(path, params=b"", headers=b""):
    """Make a GET request to the Magento API."""

    auth = OAuth1(CLIENT_KEY,
                  CLIENT_SECRET,
                  RESOURCE_OWNER_KEY,
                  RESOURCE_OWNER_SECRET)
    res = s.get(url=path, params=params, auth=auth, headers=headers)
    return res.json()


def admin_post_request(path, params=b"", headers=b""):
    """Make a POST request to the Magento API."""

    auth = OAuth1(CLIENT_KEY,
                  CLIENT_SECRET,
                  RESOURCE_OWNER_KEY,
                  RESOURCE_OWNER_SECRET)
    res = s.post(url=path, params=params, headers=headers, auth=auth)
    return res.json()


def get_order(curr_id):
    print(curr_id)
    params = {
        'searchCriteria[filter_groups][2][filters][0][field]': 'increment_id',
        'searchCriteria[filter_groups][2][filters][0][value]': curr_id,
        'searchCriteria[filter_groups][2][filters][0][condition_type]': 'eq'
    }
    return admin_get_request(f'{BASE_URL}/orders', params=params)


def create_shipment(order_id,notify=True):
    payload = {
        "notify": notify
    }
    return admin_post_request(f'{BASE_URL}/order/{order_id}/ship', params=payload)

def is_integer(num):
    try:
        int(num)
        return True
    except:
        return False


count = 0
num_files = len([f for f in os.listdir(f"{DIRECTORY}/to-ship")
                 if os.path.isfile(os.path.join(DIRECTORY, f))])

for filename in os.listdir(DIRECTORY):
    if filename.endswith(".xml"):
        file_full_name = f"{DIRECTORY}/{filename}"
        tree = ET.parse(file_full_name)
        root = tree.getroot()
        order_id = root[0].get('id')
        try:
            order_data = get_order(order_id)
            magento_id = order_data['items'][0]['entity_id']
            print (magento_id)

            break

        except:
            print(f"- {order_id} failed. \n")
            log_file.write(f"- {order_id} failed. \n")
            os.rename(file_full_name, f"{DIRECTORY}/failed/{filename}")
            continue

        try:
            result = create_shipment(magento_id)
            if not is_integer(result):
                log_file.write(str(result))
                log_file.write(f"- {order_id} FAILED. \n")
                print(f"- {order_id} FAILED. \n")
                os.rename(file_full_name, f"{DIRECTORY}/failed/{filename}")
                continue
            
            log_file.write(str(result))
            log_file.write(f"- {order_id} succesful. \n")
            print(f"- {order_id} succesful. \n")
            os.rename(file_full_name, f"{DIRECTORY}/verwerkt/{filename}")
        except Exception as e:
            log_file.write(str(e))
            log_file.write(f"- {order_id} failed. \n")
            print(f"- {order_id} failed. \n")
            os.rename(file_full_name, f"{DIRECTORY}/failed/{filename}")
        count += 1
        print(f"- {count} of {num_files}")
print("All Done!")
log_file.close()


import os
import xml.etree.ElementTree as ET
from src.process import OrderProcessor
import src.config as config
processor = OrderProcessor()

class XmlReader:
    def read_xmls(self):

        log_file = open(config.log, "a")
        directory = config.xml_input

        count = 0
        num_files = len([f for f in os.listdir(f"{directory}")
                        if os.path.isfile(os.path.join(directory, f))])

        for filename in os.listdir(directory):
            if filename.endswith(".xml"):
                # Get order ID from XML
                file_full_name = f"{directory}/{filename}"
                tree = ET.parse(file_full_name)
                root = tree.getroot()
                order_id = root[0].get('id')

                try:
                    # Assign the Manufacturing status to the order in Magento
                    order_data = processor.processOrder(order_id)

                    # Log success
                    log_file.write(f"- {order_id} succesful. \n")
                    print(f"- {order_id} succesful. \n")

                    os.rename(file_full_name, f"{directory}/to-ship/{filename}")
                except Exception as e:
                    print(e)
                    log_file.write(str(e))
                    log_file.write(f"- {order_id} failed. \n")
                    print(f"- {order_id} failed. \n")

                    # os.rename(file_full_name, f"{directory}/failed/{filename}")
                count += 1
                print(f"- {count} of {num_files}")
        print("All Done!")
        log_file.close()

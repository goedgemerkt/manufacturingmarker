import requests
from requests_oauthlib import OAuth1
import os
import json
import src.config as config

env = config.environments[config.current]

s = requests.Session()

class OrderProcessor:
    def __init__(self):
        self.baseUrl = env['base_url']

        self.clientKey = env['client_key']
        self.clientSecret = env['client_secret']
        self.tokenKey = env['resource_owner_key']
        self.tokenSecret = env['resource_owner_secret']

    def admin_get_request(self, path, params=b"", headers=b""):
        """Make a GET request to the Magento API."""

        auth = OAuth1(self.clientKey,
                      self.clientSecret,
                      self.tokenKey,
                      self.tokenSecret)
        res = s.get(url=path, params=params, auth=auth, headers=headers)
        return res.json()

    def admin_post_request(self, path, data=b"", headers=b""):
        """Make a POST request to the Magento API."""
        auth = OAuth1(self.clientKey,
                      self.clientSecret,
                      self.tokenKey,
                      self.tokenSecret)

        body = json.dumps(data)
        res = s.post(url=path, data=body, auth=auth, headers={
            'Content-Type': 'application/json'
        })
        return res

    def get_order(self, currID):
        params = {
            'searchCriteria[filter_groups][2][filters][0][field]': 'increment_id',
            'searchCriteria[filter_groups][2][filters][0][value]': currID,
            'searchCriteria[filter_groups][2][filters][0][condition_type]': 'eq'
        }
        return self.admin_get_request(f'{self.baseUrl}/orders', params=params)

    def change_status(self, order_id, increment_id):
        payload = {
            "entity": {
                "entity_id": order_id,
                "status": "manufacturing",
                "increment_id": increment_id
            }
        }
        return self.admin_post_request(f'{self.baseUrl}/orders', data=payload)

    def processOrder(self, order):
        order_data = self.get_order(order)

        magento_id = order_data['items'][0]['entity_id']

        res = self.change_status(magento_id, order)

        if res.status_code != 200:
            print(res.json())
            raise Exception(res)

        return res.json()
